package mt.edu.mcast.loginintents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    final int LOGIN_ACTIVITY = 5;
    TextView tvMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvMsg = findViewById(R.id.txt_msg);
        Button btnLogin = findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), SecondActivity.class);

                EditText uname = findViewById(R.id.etxt_uname);
                EditText pass = findViewById(R.id.etxt_pass);

                i.putExtra("uname", uname.getText().toString());
                i.putExtra("password", pass.getText().toString());

                startActivityForResult(i, LOGIN_ACTIVITY);
            }
        });
    }

    public void onActivityResult(int reqCode, int resCode, Intent data){

        if(reqCode == LOGIN_ACTIVITY){

            if(resCode == RESULT_CANCELED){

                tvMsg.setText("Invalid Credentials");

            }else if (resCode == RESULT_OK) {
                tvMsg.setText("");
            }

        }

    }
}
