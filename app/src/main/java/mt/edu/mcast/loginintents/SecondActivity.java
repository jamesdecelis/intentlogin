package mt.edu.mcast.loginintents;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class SecondActivity extends AppCompatActivity {

    int result = RESULT_CANCELED;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Bundle extras = getIntent().getExtras();

        if(extras != null){
            String uname = extras.getString("uname");
            String pass = extras.getString("password");

            if( !uname.equals("james") || !pass.equals("123")){

                result = RESULT_CANCELED;
                finish();

            }

        }

    }

    public void backButton(View v){

        result = RESULT_OK;
        finish();

    }

    public void finish(){

        setResult(result);

        super.finish();

    }

}
